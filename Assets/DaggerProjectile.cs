﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DaggerProjectile : MonoBehaviour
{
    [SerializeField] private Collider2D bladeCollider, gripCollider;
    [SerializeField] private float Force = 10F;
    [SerializeField] private float Torque = 10F;
    [SerializeField] private float BounceDrag = 3F;
    [SerializeField] private float StuckInset = 0.15F;
    [SerializeField] private float PickupTime = 1F;

    private float StartTime = 0F;
    private Rigidbody2D rb;
    private State state = State.inFlight;

    public State CurrentState => state;
    public bool CanBePickedUp { get; private set; }

    public enum State
    {
        inFlight,
        bounced,
        stuck
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        rb.AddForce(transform.up * Force, ForceMode2D.Impulse);
        rb.AddTorque(Torque, ForceMode2D.Impulse);
        StartTime = Time.time;

        IgnoreCollisions(true);
    }

    private void FixedUpdate()
    {
        rb.AddForce(Wind.Instance.WindDirection);

        if (Time.time - PickupTime > StartTime)
        {
            CanBePickedUp = true;
        }
    }

    private void IgnoreCollisions(bool value)
    {
        var pc = Player.Instance.GetComponent<Collider2D>();
        Physics2D.IgnoreCollision(pc, bladeCollider, value);
        Physics2D.IgnoreCollision(pc, gripCollider, value);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        var player = collision.collider.GetComponent<Player>();
        if (player != null && CanBePickedUp)
        {
            player.GetComponent<Dagger>().PickupAmmo(1);
            Destroy(this.gameObject);
        }

        if (state == State.stuck)
            return;

        var contact = collision.contacts[0];
        var hitCollider = contact.otherCollider;
        var normal = new Vector3(contact.normal.x, contact.normal.y, 0F);

        if (state == State.inFlight)
        {
            if (hitCollider == bladeCollider)
            {
                transform.position += -normal * StuckInset;
                rb.velocity = Vector2.zero;
                rb.constraints = RigidbodyConstraints2D.FreezeAll;
                state = State.stuck;
                CanBePickedUp = true;
                IgnoreCollisions(false);
            }
            else if (hitCollider == gripCollider)
            {
                rb.drag = BounceDrag;
                state = State.bounced;
                CanBePickedUp = true;
                IgnoreCollisions(false);
            }
        }
    }
}
