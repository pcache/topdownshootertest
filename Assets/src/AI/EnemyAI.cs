﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyAI : MonoBehaviour
{
    protected EnemyAICtrl ctrl { get; private set; }

    public List<FSMState> states = new List<FSMState>();

    public virtual void Initialize(EnemyAICtrl ctrl)
    {
        this.ctrl = ctrl;
    }
}
