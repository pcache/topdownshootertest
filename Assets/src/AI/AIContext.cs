﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AIContext : MonoBehaviour
{
    public float LineOfSightAngle = 45F;
    public float LineOfSightDistance = 10F;


    public Vector3 position;
    public Vector3 rotation;
    public float rotationZ;

    public Transform targetTransform;
    public Vector3 positionTarget;
    public Vector3 directionTarget;
    public WaypointPath waypointPath;
    public float waypointNextDistance = 0.2F;

    public float Speed = 2F;

}


