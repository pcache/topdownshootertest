﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkForward : FSMState
{
    public override State Execute(AIContext context)
    {
        context.position += context.transform.up * Time.deltaTime;
        return State.success;
    }
}