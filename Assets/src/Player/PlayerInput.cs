﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum KeyState
{
    none = 0,
    up = 1,
    down = 2,
    pressed = 3
}

public class PlayerInput : MonoBehaviour
{
    public bool PlayerInputActive { get; set; } = true;

    private long frame;
    private InputState lastInput;

    public struct InputState
    {
        public float vertical, horizontal;
        public bool hasMovementInput, hasLookInput;
        public float lookX, lookY;
        public Vector3 mousePosition, mouseWorldPosition, movementVector, lookVector;
        public KeyState attack, dash, run;
        public KeyState weapon1, weapon2;

    }

    private InputState EmptyInputState => new InputState();

    public InputState GetInput()
    {
        if (!PlayerInputActive)
            return EmptyInputState;

        int currentFrame = Time.frameCount;

        if (currentFrame == frame)
            return lastInput;

        KeyState resolveKeyState(KeyCode code)
        {
            if (Input.GetKeyDown(code))
                return KeyState.down;

            if (Input.GetKeyUp(code))
                return KeyState.up;

            if (Input.GetKey(code))
                return KeyState.pressed;

            return KeyState.none;
        }

        lastInput = new InputState()
        {
            vertical = Input.GetAxisRaw("Vertical"),
            horizontal = Input.GetAxisRaw("Horizontal"),
            lookX = Input.GetAxisRaw("Mouse X"),
            lookY = Input.GetAxisRaw("Mouse Y"),
            mousePosition = Input.mousePosition,
            attack = resolveKeyState(KeyCode.Mouse0),
            dash = resolveKeyState(KeyCode.Mouse1),
            run = resolveKeyState(KeyCode.LeftShift),
            weapon1 = resolveKeyState(KeyCode.Alpha1),
            weapon2 = resolveKeyState(KeyCode.Alpha2),
            mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition),
        };

        lastInput.hasMovementInput =    lastInput.vertical != 0F || lastInput.horizontal != 0F;
        lastInput.hasLookInput     =    lastInput.lookX != 0F    || lastInput.lookY != 0F;
        lastInput.movementVector = new Vector2(lastInput.horizontal, lastInput.vertical);
        lastInput.lookVector = new Vector2(lastInput.lookX, lastInput.lookY);

        frame = currentFrame;

        return lastInput;
    }

}
