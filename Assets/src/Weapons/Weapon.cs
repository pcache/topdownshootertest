﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public float Cooldown = 0.1F;
    public float TimeSinceLastAttack;
    public int AmmoMax;
    public int AmmoCurrent;

    [SerializeField] public GameObject ProjectilePrefab;
    [SerializeField] private Transform launchPoint;
    [SerializeField] private Rigidbody2D playerRB;

    private void Awake()
    {
        playerRB = Player.Instance.GetComponent<Rigidbody2D>();
    }

    public virtual void Attack()
    {
        if (AmmoCurrent > 0)
        {
            LaunchProjectile();
            TimeSinceLastAttack = Time.time;
        }
    }

    protected virtual void LaunchProjectile()
    {
        if (launchPoint == null || ProjectilePrefab == null)
            return;

        var projectile = Instantiate(ProjectilePrefab);

        projectile.transform.position = launchPoint.transform.position;
        projectile.transform.up = launchPoint.transform.up;
        var rb = projectile.GetComponent<Rigidbody2D>().velocity = playerRB.velocity;
        AmmoCurrent--;
    }

    public virtual void PickupAmmo(int count)
    {
        AmmoCurrent = Mathf.Clamp(AmmoCurrent + count, 0, AmmoMax);
    }
}