﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetsCtrl : MonoBehaviour
{
    [SerializeField] private GameObject targetPrefab;
    [SerializeField] private int MaxActiveTargets = 3;

    private Transform[] allSpawnPoints;
    private List<Transform> unusedPoints = new List<Transform>();

    private int ActiveTargets;

    private void Awake()
    {
        allSpawnPoints = GetComponentsInChildren<Transform>();
        unusedPoints.AddRange(allSpawnPoints);
    }

    private void Update()
    {
        if(ActiveTargets < MaxActiveTargets)
        {
            for (int i = 0; i < MaxActiveTargets - ActiveTargets; i++)
            {
                SpawnTarget();
            }
        }
    }

    private void SpawnTarget()
    {
        int count = unusedPoints.Count;

        var point = unusedPoints[UnityEngine.Random.Range(0, count)];

        var target = Instantiate(targetPrefab, point).GetComponent<Target>();
        target.transform.localPosition = Vector3.zero;

        target.OnHit += x =>
        {
            unusedPoints.Add(x);
            ActiveTargets--;
        };

        ActiveTargets++;
    }
}
