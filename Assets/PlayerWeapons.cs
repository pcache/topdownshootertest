﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapons : MonoBehaviour
{
    [SerializeField] private List<Weapon> weapons;
    [SerializeField] private Transform projectileLaunchPoint;
    private PlayerInput playerInput;
    private Weapon currentWeapon;


    private void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
        currentWeapon = weapons[0];
    }

    private void Update()
    {
        var input = playerInput.GetInput();
        float dt = Time.deltaTime;

        Vector3 mousePos = input.mouseWorldPosition;
        mousePos.z = projectileLaunchPoint.position.z;
        projectileLaunchPoint.up = (mousePos - projectileLaunchPoint.position ).normalized;

        void SetWeapon(int index)
        {
            currentWeapon = weapons[index];
        }

        if (input.weapon1 == KeyState.down)
            SetWeapon(0);
        //else if(input.weapon2 == KeyState.down)
        //    SetWeapon(1);

        if(input.attack == KeyState.down)
        {
            if(Time.time - currentWeapon.Cooldown > currentWeapon.TimeSinceLastAttack)
            {
                currentWeapon.Attack();
            }
        }


    }
}
