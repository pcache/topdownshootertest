﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (!instance)
            {
                instance = GameObject.FindObjectOfType<T>();
            }
            return instance;
        }
    }

    protected virtual void Awake()
    {
        if(instance != null)
        {
            Debug.LogError($"Singleton {typeof(T).Name} already exists, fix it. You probably forgot initializer",gameObject);
        }

        instance = this as T;
    }
}